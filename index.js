var extend = require('extend');

var config_defaults = {
  "paths": {
    "source": "./src",
    "destination": "./dest",
    "src": {
      "js": "(src)/js",
      "sass": "(src)/sass",
      "images": "(src)/images",
      "vendor": "(src)/vendor"
    },
    "dest": {
      "js": "(dest)/js",
      "css": "(dest)/css",
      "images": "(dest)/images",
      "vendor": "(dest)/vendor"
    }
  },
  "globs": {
    "js": "(js)/**/*.js",
    "sass": "(sass)/**/*.scss",
    "images": "(images)/**/*.{jpg,png,gif,svg}"
  }
};

module.exports = function(config) {

  var __config__ = {};
  
  function setConfig(config)
  {
    __config__ = extend(__config__, config_defaults, config || {});
    
    // replace wildcards
    __config__.paths.src = replaceWildcards(__config__.paths.src, {src: __config__.paths.source});
    __config__.paths.dest = replaceWildcards(__config__.paths.dest, {dest: __config__.paths.destination});
    __config__.globs = replaceWildcards(__config__.globs, __config__.paths.src);
  }

  setConfig(config || {});

  // =======================================================

  /**
   * [replaceWildcards description]
   *
   *  NOTE only replaces one prefix at start of string
   * 
   * @param  {[type]} strings                [description]
   * @param  {[type]} replacements           [description]
   * @param  {[type]} optionalWildcardPrefix [description]
   * @return {[type]}                        [description]
   */
  function replaceWildcards(strings, replacements, optionalWildcardPrefix)
  {
    if(optionalWildcardPrefix && typeof(optionalWildcardPrefix) === 'string') {
      // ensure strings that start with no prefix or directory path
      // start with the nominated prefix
      strings = optionalWildcardPrefix(strings, optionalWildcardPrefix);
      
      // call again
      return replaceWildcards(strings, replacements);
    }

    isString = false;
    if(typeof(strings) === 'string') {
      isString = true;
      strings = [strings];
    }

    _replace = function(i, k)
    {
      var prefix = '('+k+')';
      // NOTE only replaces one prefix at start of string
      if(strings[i].indexOf(prefix) === 0) {
        strings[i] = strings[i].replace(prefix, replacements[k]);
      }
    }

    if(Object.prototype.toString.call(strings) === '[object Array]') {
      for(var i=0,c=strings.length; i<c; i++) {
        for(var k in replacements) {
          if(replacements.hasOwnProperty(k)) {
            _replace(i,k);
          }
        }
      }
    } else { // assumption : object
      for(var i in strings) {
        if(strings.hasOwnProperty(i)) {
          for(var k in replacements) {
            if(replacements.hasOwnProperty(k)) {
              _replace(i,k);
            }
          }
        }
      }
    }

    return isString ? strings[0] : strings;
  }

  // =======================================================

  /**
   * [wildcardPrefix description]
   * @param  {[type]} strings  [description]
   * @param  {[type]} wildcard [description]
   * @return {[type]}          [description]
   */
  function wildcardPrefix(strings, wildcard)
  {
    isString = false;
    if(typeof(strings) === 'string') {
      isString = true;
      strings = [strings];
    }

    var _strPrefix = function(i)
    {
      strings[i] = _prefixPath(strings[i], wildcard);
    }

    if(Object.prototype.toString.call(strings) === '[object Array]') {
      for(var i=0,c=strings.length; i<c; i++) {
        _strPrefix(i);
      }
    } else { // assumption : object
      for(var i in strings) {
        if(strings.hasOwnProperty(i)) {
          _strPrefix(i);
        }
      }
    }

    return isString ? strings[0] : strings;
  }

  // private
  var _protectPath = /^[\.]{0,2}\//;
  function _prefixPath(string, wildcard)
  {
    if(string[0] == '(') {
      // it has a wildcard ex. `"(vendor)"`
      return string;
    }
    
    //if(string[0] != '/' && string.slice(0,2) != './' && string.slice(0,3) != '../') {
    if( ! _protectPath.test(string[0])) {  
      string = wildcard + '/' + string;
    }
    return  string;
  }

  // =======================================================
  // exported
  // 
  return {
    config: function(key)
    {
      // return top level confic property
      return __config__[key];
    },
    replaceWildcards: replaceWildcards,
    wildcardPrefix: wildcardPrefix,
    extend: extend
  };
}