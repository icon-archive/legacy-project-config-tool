
DOCUMENT STATUS - INCOMPLETE / UNCHECKED

---

# Project Config Tool

A configurable object of low level project tools.

## Primary Purpose

Tool for dealing with project path strings and globs 
to be used in `gulpfile.js` but it is NOT a gulp plugin per say.

### Other puposes

If other purposes are ever added they will only be done so 
if no other obvious method is available anywhere in the local variables. 
ie. use whats available in the other plugins you may have loaded.

---

## Require the tool

NOTE: it must be called (it is a function). 99% of the time
      you will be configuring it so it's simple to do it
      while you `require`.

Get your additional project config json.

```
var conf;
try {
  conf = require('project-config.json');
}catch(error) {
  // dont care
  conf = {};
}
```

Require the project tool.

```
var project = require('project-config-tool')(conf);
```

If you ommit the `conf` object you will just get the default.
The projects config is merged with default via an extend.
`extend` has the same signature as jQuery's `.extend`.

### Wildcards

Wildcards in strings are extremely simple.
There can only be up to one wildcard per string (0 - 1)
and is matched at the start.
This by design.
There is no regular expression to replace multiples as the
currently purpose of the tool does not require it.
If multiple replaces are ever added single will still be 
the default.

A wildcard is a key in brackets `(key)`.

It is replaced by the value of .key in object provided to
one of the utility functions.

#### Available wildcards

* `"(src)"`
* `"(dest)"`
* `"(js)"`         
* `"(css)"`
* `"(sass)"`
* `"(images)"`
* `"(vendor)"`

### Config transformation

When the project tool initialises it extends the provided
`conf` into the project config then it replaces the wildcards
in `paths` and `globs`.

see: Config structure bellow

### `paths` (Object)

Get the paths object `var paths = project.config('paths')`

#### `paths.dest` (Object)

* `js`
* `css`
* `images`
* `vendor`

#### `paths.src` (Object)

* `js`
* `sass`
* `images`
* `vendor`

You rarely need to access these directly when using gulp. 
The default convinience `globs` resolve their willcards to this object.

### `globs`

* `js`
* `sass`
* `images`

A vendor glob is not provided by default as they should typically
be consumed into builds. For bespoke needs wite seperate tasks.

-----------------------------------

## Project tool properties

Examples are for gulp as thats the primary purpose of this tool.

### `config(key)`

* `@param key` _String_

Returns root level config _Object_.

Example: get project `paths` and `globs`

```
var paths = project.config('paths');
var globs = project.config('globs');

gulp.task('sass', function(){
  return gulp.src( globs.sass )
    .pipe(sass().on('error', sass.logError))
    .pipe( gulp.dest( paths.dest.css ) );
});
```

### `replaceWildcards(strings, replacements)`

* `@param` _String_ or (_Array_ or _Object_) of _String_s
* `@param` _Object_ of wildcard replacements

Replace wildcards with the values in replacements.

Example: a build of js files

```
var buildConfig = require( paths.src.js + 'build.json' );
```

Returns: 
* `outfile` _String_ ex. "main.js"
* `files` _Object_ of path _Strings_

Prefix paths with `"(js)"` wildcard.
Paths that already start with a wildcard or `"/"` or `"./"` or `"../"` 
will remain intact.

`project.wildcardPrefix(buildConfig.files, "(js)");`

```
gulp.task('main-js', function(){
```
Replace the `files` glob wildcards
```
  var files = project.replaceWildcards(buildConfig.files, paths.src);
```
Concatenate files and save destination js
```
  return gulp.src(files)
    .pipe(concat( buildConfig.outfile ))
    .pipe( gulp.dest( paths.dest.js ) );
});
```

### `wildcardPrefix(strings, wildcard)`

Add wildcard to start of strings.

Strings that already start with a wildcard or `"/"` or `"./"` 
or `"../"` will remain intact.

### `extend([deep,] target, object [, ... more objects ...])`

Same signature as `jQuery.extend`

Used internally so may as well expose, extend is useful.

-----------------------------------
## Config structure

legend: `"key"`: _"default"_

* `"paths"`: _Object_
  * `"source"`: _"./src"_ where you understand your sass/js sources root to be
  * `"destination"`: _"./dest"_ where you understand you public/production to be
  * `"src"`: _Object_ source paths - the wildcard `"(src)"` will resolve to the value of `paths.source`
  * `"dest"` _Object_ destination paths - the wildcard `"(dest)"` will resolve to the value of `paths.destination`
* `"globs"`: _Object_
  * keys in `paths.src` can be used as wildcards here


